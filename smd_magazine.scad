// tape dimensions and tolerance
tape_width=16;
tape_thickness=2.5;
tolerance=0.4;

// overall size (width determined by tape width)
overall_depth=50;
overall_height=60; // should be > overall_depth

// magnet size 
use_magnets=1; // 0 to omit
magnet_diameter=3;
magnet_thickness=1;
magnet_tolerance=0.2;

// hub hole size
hub_hole_size=15;
hub_size_tweak=1;

// DIN rail mount
use_din_mount=1; // 0 to omit

overall_width=tape_width+2+tolerance;

//rail_mockup(); // comment out for production

rotate([0, -90, 0])
    body();

translate([-overall_height/2, 1.5*overall_depth+5, 0])
    lid();

module rail_mockup()
{
    translate([5.2, 19.5, 30])
    rotate([0, 180, 0])
    {
        include <din_rail.scad>
    }
}

module body()
{
    difference()
    {
        // main body
        cube([overall_width, overall_depth, overall_height]);

        // tape well
        translate([1, overall_depth/2, overall_height/2])
        rotate([0, 90, 0])
            cylinder(d=overall_depth-4, h=1+tape_width+tolerance, $fn=360);

        // chamfered hole for lid
        translate([0, overall_depth/2, overall_height/2])
        rotate([0, 90, 0])
        rotate_extrude($fn=360) // chamfer tool
            polygon([[0, 0], [hub_hole_size/2+.5, 0], [hub_hole_size/2, .5], [hub_hole_size/2+.5, 1], [0, 1]]);

        // parts window on top
        translate([1+tolerance/2, 8, overall_height-1-tape_thickness-tolerance])
            cube([1+tape_width+tolerance, 17, 1+tape_thickness+tolerance]);

        // tape slot at front
        translate([1, 0, overall_height-1-tape_thickness-tolerance])
            cube([tape_width+tolerance, overall_depth/2, tape_thickness+tolerance]);

        // 15 mm radius at upper back
        translate([0, overall_depth-15, overall_height-15])
        rotate([90, 0, 90])
        difference()
        {
            cube([15, 15, 2+tape_width+tolerance]);
            cylinder(r=15, h=2+tape_width+tolerance, $fn=360);
        }

        // tape path to well
        translate([1, overall_depth/2, overall_height-(overall_depth-4)/2-1])
        rotate([90, 0, 90])
        difference()
        {
            cylinder(d=overall_depth-4, h=1+tape_width+tolerance, $fn=360);
            translate([0, tape_thickness+tolerance, 0])
                cylinder(d=overall_depth-4-(tape_thickness+tolerance)*4, h=1+tape_width+tolerance, $fn=360);
            translate([-(overall_depth-4)/2, -(overall_depth-4)/2, 0])
            {
                cube([(overall_depth-4)/2, overall_depth-4, 1+tape_width+tolerance]);
                cube([overall_depth-4, (overall_depth-4)/2, 1+tape_width+tolerance]);
            }
            translate([0, (overall_depth-4)/2-4, tape_width+tolerance])
                cube([4, 4, 4]);
        }
        translate([1, overall_depth-5, overall_height/2])
            cube([1+tape_width+tolerance, 3, (overall_height-overall_depth)*.75]);

        // film slot at top rear
        translate([1, 25, overall_height-.25])
            cube([tape_width+tolerance, overall_depth-25, .25]);

        // label pocket on front
        if (use_din_mount)
        {
            translate([1, 0, 9])
                cube([tape_width+tolerance, .25, overall_height-3-tape_thickness-tolerance-8]);
        }
        else
        {
            translate([1, 0, 1])
                cube([tape_width+tolerance, .25, overall_height-3-tape_thickness-tolerance]);
        }

        // label pocket on back
        translate([1, overall_depth-.25, 1])
            cube([tape_width+tolerance, .25, overall_height-3-tape_thickness-tolerance-15]);

        // magnets
        if (use_magnets)
        {
            translate([0, (overall_height-overall_depth)/2, overall_height-overall_depth])
            rotate([0,90,0])
                cylinder(d=magnet_diameter+magnet_tolerance, h=magnet_thickness+magnet_tolerance, $fn=60);
            translate([0, overall_depth-(overall_height-overall_depth)/2, overall_height-overall_depth])
            rotate([0,90,0])
                cylinder(d=magnet_diameter+magnet_tolerance, h=magnet_thickness+magnet_tolerance, $fn=60);
            translate([0, (overall_height-overall_depth)/2, overall_depth])
            rotate([0,90,0])
                cylinder(d=magnet_diameter+magnet_tolerance, h=magnet_thickness+magnet_tolerance, $fn=60);
            translate([overall_width-magnet_thickness-magnet_tolerance, (overall_height-overall_depth)/2, overall_height-overall_depth])
            rotate([0,90,0])
                cylinder(d=magnet_diameter+magnet_tolerance, h=magnet_thickness+magnet_tolerance, $fn=60);
            translate([overall_width-magnet_thickness-magnet_tolerance, overall_depth-(overall_height-overall_depth)/2, overall_height-overall_depth])
            rotate([0,90,0])
                cylinder(d=magnet_diameter+magnet_tolerance, h=magnet_thickness+magnet_tolerance, $fn=60);
            translate([overall_width-magnet_thickness-magnet_tolerance, (overall_height-overall_depth)/2, overall_depth])
            rotate([0,90,0])
                cylinder(d=magnet_diameter+magnet_tolerance, h=magnet_thickness+magnet_tolerance, $fn=60);
        }

        // DIN mount
        if (use_din_mount)
        {
            translate([0, 3, 0]) // front latch
                cube([overall_width, 6, 2.75]);
            translate([0, 1, 1])
            {
                cube([overall_width, 5, 1.75]);
                cube([overall_width, 1, 7.5]);
            }

            rotate([-55,0,0]) // pry slot for front latch
            translate([overall_width/2-2, -5.5, 1.5])
                cube([4, 6, 1]);

            translate([0, 31.5, 0]) // back notch
                cube([overall_width, 4, 2.75]);
            translate([0, 35.25, 1])
                cube([overall_width, 2, 1.75]);
        }
    }

    // tape path to well
    translate([1+tape_width+tolerance, 25, overall_height-1-tape_thickness-2])
        cube([1, overall_depth/2-21, tape_thickness+2]);

    // film slot at top rear
    translate([0, overall_depth-8, overall_height-1])
        cube([2+tape_width+tolerance, 3, 1]);
    translate([0, overall_depth-8, overall_height-5])
        cube([1, 3, 5]);
    translate([overall_width-1, overall_depth-8, overall_height-5])
        cube([1, 3, 5]);
}


module lid()
{
    difference()
    {
        union() // basic disc and hub
        {
            cylinder(d=overall_depth-5, h=1, $fn=360);    
            cylinder(d=hub_hole_size+hub_size_tweak, h=overall_width, $fn=360);    
        }
        cylinder(d=12, h=overall_width, $fn=360);

        translate([-1, -8, 1]) // cut hub into pieces for flexibility   
            cube([2, 16, overall_width]);
        translate([-8, -1, 1])
            cube([16, 2, overall_width]);

        translate([0, 0, overall_width-1])
        rotate_extrude($fn=360) // inverse of the previous chamfer tool
            polygon([[overall_depth/2, 0], [(hub_hole_size+hub_size_tweak)/2, 0], [(hub_hole_size+hub_size_tweak)/2-.5, .5], [(hub_hole_size+hub_size_tweak)/2, 1], [overall_depth/2, 1]]);

        // cut out windows
        rotate_extrude(angle=90, $fn=360)
            polygon([[(hub_hole_size+hub_size_tweak+5)/2, 0], [(overall_depth-5)/2-5, 0], [(overall_depth-5)/2-5, 1], [(hub_hole_size+hub_size_tweak+5)/2, 1], [(hub_hole_size+hub_size_tweak+5)/2, 0]]);
        rotate([0,0,120])
        rotate_extrude(angle=90, $fn=360)
            polygon([[(hub_hole_size+hub_size_tweak+5)/2, 0], [(overall_depth-5)/2-5, 0], [(overall_depth-5)/2-5, 1], [(hub_hole_size+hub_size_tweak+5)/2, 1], [(hub_hole_size+hub_size_tweak+5)/2, 0]]);
        rotate([0,0,240])
        rotate_extrude(angle=90, $fn=360)
            polygon([[(hub_hole_size+hub_size_tweak+5)/2, 0], [(overall_depth-5)/2-5, 0], [(overall_depth-5)/2-5, 1], [(hub_hole_size+hub_size_tweak+5)/2, 1], [(hub_hole_size+hub_size_tweak+5)/2, 0]]);
    }
}
