use <fontmetrics.scad>

$fn=45;

label("10kΩ 0603", "Arial", 5, .75);

translate([0,10,0])
label("10", "Arial:style=Bold", 5, 1);

module label(t, f, h, xc)
{
    l=measureText(t, font=f, size=h)+2;

    scale([xc, 1, 1])
    translate([0.5,2,.4])
    linear_extrude(.6, convexity=10)
        text(t, size=h, font=f);

    cube([l*xc, 8, 0.4]);
}
