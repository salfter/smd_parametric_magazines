Parametric SMD Storage Cartridges
=================================

There are lots of SMD storage solutions out there that promise
customizability (including [this
one](https://www.printables.com/model/168618-smd-storage-cartridges-with-din-rail-dock/)),
but every one I've run across requires expensive (or at best,
free-as-in-beer) proprietary software that would also amount to yet another
package to have to learn how to use when all I want is to put some
organization to the parts I've accumulated for various projects.  This one's
different: it uses OpenSCAD to generate storage cartridges for whatever size
of SMD tape you need, kn whatever capacity, and with options to hold
multiple cartridges together with magnets and/or a DIN rail.  It borrows
several ideas from similar gadgets I've seen elsewhere.

The source file is reasonably well-commented so that it should be easy to
figure out what to tweak to produce the sizes you need/want.

I've also included a script to produce thin, quick-printing labels that you
can glue on, since 8mm paper labels for label printers seem to be hard to
come by.  They're intended to be printed in two colors: the first two layers
in the background color you want, then the last three in the text color you
want.

You can also find this project at
[Printables.com](https://www.printables.com/model/303558-parametric-smd-storage-cartridges-openscad).

din_rail.scad is from
[Printables.com](https://www.printables.com/model/193512-openscad-din-rail)
and is included for reference.

fontmetrics.scad is from
[Thingiverse](https://www.thingiverse.com/thing:3004457) and is used by the
labeler script.

