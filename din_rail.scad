//variables
length = 100;// length of the DIN RAIN (MULTIPLES OF 10mm!)

hole = 4; //hole diamter

//code

translate([6.5,25/2,0])
    cube([1,5,length]);
    
translate([0,25/2,0])
    cube([7.5,1,length]);;

mirror([0,1,0]){
   
translate([6.5,25/2,0]){
    cube([1,5,length]);}
translate([0,25/2,0]){
    cube([7.5,1,length]);};
;}

for(i= [10:10:length])
{
difference()
{
    translate([0,-25/2,i-10])
        cube([1,25,10]);
    b = i-5;
    translate([-2,0,b])
        rotate([0,90,0])  
            cylinder(4,hole/2,hole/2, $fn=30);
    
}
 }  
    